CC = cl
CFLAGS = /nologo /MD
PGR = so-cpp.exe

build: $(PGR)

$(PGR): main.obj hash.obj utils.obj
	$(CC) $(CFLAGS) $** /Fe$@

main.obj: main.c hash.h utils.h
	$(CC) $(CFLAGS) /Fomain.obj /c main.c

hash.obj: hash.c hash.h
	$(CC) $(CFLAGS) /Fohash.obj /c hash.c
	
utils.obj: utils.c utils.h hash.h
	$(CC) $(CFLAGS) /Foutils.obj /c utils.c


clean:
	del *.obj $(PGR)