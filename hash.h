#ifndef HASH_H
#define HASH_H


#define HASH_SIZE 1023
#define BUF_SIZE 1024
#define NO_OF_LINES 30
#define ERROR_MALLOC 12  
#define OK 1
#define NOT_OK -1
#define VALUE_HASH 5381
#define VALUE_MULTIPLIER 3
#define ZERO 0

typedef struct node {
	char  *value;
	struct node *next;
} node;


typedef struct hash {
	node **head;
	int length;
} hash_struct;


int create_hash(hash_struct **hash_map);

int hash_function(const char *str);

node *find(char *search_term, hash_struct *hash_map);

int add(char *symbol, char *value_to_add, hash_struct *hash_map);

int rem(char *value, hash_struct *hash_map);

void clear(hash_struct *hash_map);

void free_hash(hash_struct *hash_map);


#endif
