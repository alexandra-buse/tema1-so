#include "hash.h"

#ifndef UTILS_H
#define UTILS_H

#define NEW_LINE 0
#define DEFINE 1
#define UNDEF 2
#define IF 3
#define EN 4
#define ELS 5
#define ELF 6
#define IFDEF 7
#define IFNDEF 8
#define INCLUDE 9
#define DEFAULT 10


char *strremove(char *str, const char *sub);

char *check_token(char *token, hash_struct *map);

void rep_tok(char *token, char *temp_buf, char *new_v, char *replace);

int check_define_value(char *value, hash_struct *map);

int add_define_to_map(hash_struct *map);

int delete_define_from_map(hash_struct *map);

int check_if(hash_struct *map);

int find_defined_token(hash_struct *map);

int change_value(int flag);

int replace(char *str, hash_struct *map, char *result, int *flag, char *path);

int print_file(char *filename, hash_struct *map, char *path);

void add_param_to_map(char *str, hash_struct *map);

int decode_param(int argc, char **argv, hash_struct *map);

int do_include(hash_struct *map, char *result, int *flag, char *path);


#endif
