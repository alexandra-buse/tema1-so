#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

/* Functie pentru eliminarea unui subsir dintr-un sir */
char *strremove(char *str, const char *sub)
{
	size_t len = strlen(sub);
	char *p;

	if (len > 0) {
		p = str;

		while ((p = strstr(p, sub)) != NULL)
			memmove(p, p + len, strlen(p + len) + 1);
	}
	return str;
}

/* Se verifica daca tokenul exista in HashMap */
char *check_token(char *token, hash_struct *map)
{
	node *np;

	np = find(token, map);
	if (np != NULL)
		return np->value;
	return NULL;
}


/* Se inlocuieste tokenul cu valoarea corespunzatoare */
void rep_tok(char *token, char *temp_buf, char *new_v, char *replace)
{
	int i, cnt = 0, new_len = strlen(new_v), old_len = strlen(token);
	char *result;

	for (i = 0; temp_buf[i] != '\0'; i++) {
		if (strstr(&temp_buf[i], token) == &temp_buf[i]) {
			cnt++;
			i += old_len + 1;
		}
	}

	result = (char *)malloc(i + cnt * (new_len - old_len) + 1);

	i = 0;

	while (*temp_buf) {
		if (strstr(temp_buf, token) == temp_buf) {
			strcpy(&result[i], new_v);

			i += new_len;
			temp_buf += old_len;
		} else {
			result[i++] = *temp_buf++;
			}
	}
	result[i] = '\0';
	strcpy(replace, result);
	free(result);
}

/* Se verifica valoarea din define */
/* pentru a vedea daca trebuie adaugata in HashMap */

int check_define_value(char *value, hash_struct *map)
{
	char *token, *found, *delim = " ";
	char *temp = malloc(BUF_SIZE), *result = malloc(BUF_SIZE);

	if (temp == NULL)
		return ERROR_MALLOC;

	if (result == NULL)
		return ERROR_MALLOC;

	strcpy(temp, value);
	token = strtok(temp, delim);
	while (token != NULL) {
		found = check_token(token, map);
		if (found != NULL) {
			rep_tok(token, value, found, result);
			strcpy(value, result);
		}
		token = strtok(NULL, delim);
	}

	free(result);
	free(temp);
	return OK;

}

/* Se adauga in HashMap valoarea simbolului definit */
int add_define_to_map(hash_struct *map)
{
	int err;
	char *symbol, *value;
	char *delm = " \t();=<>.,[]/\n";
	char *delm2 = "\t();=<>.,[]/\n";

	symbol = strtok(NULL, delm);
	value = strtok(NULL, delm2);

	err = check_define_value(value, map);
	if (err == ERROR_MALLOC)
		return ERROR_MALLOC;

	add(symbol, value, map);
	return OK;
}



/*Se sterge o variabila din HashMap */
int delete_define_from_map(hash_struct *map)
{
	char *token, *delim = "\n ";
	int err = OK;

	token = strtok(NULL, delim);
	err = rem(token, map);
	return err;

}


/* Se verifica valoarea cuvantului aflat */
/* dupa #if, #else etc si se returneaza valoarea */
/* acestuia convertita in int */

int check_if(hash_struct *map)
{
	char *token, *replace, *delim = "\n ";

	token = strtok(NULL, delim);
	replace = check_token(token, map);
	if (replace != NULL)
		return atoi(replace);

	return atoi(token);
}

/*Se verifica daca tokenul este definit si se returneaza O sau 1 */
int find_defined_token(hash_struct *map)
{
	char *delim = "\n ";
	char *token;
	node *p;

	token = strtok(NULL, delim);
	p = find(token, map);
	if (p != NULL)
		return OK;

	return ZERO;
}


int change_value(int flag)
{
	if (flag == ZERO)
		return OK;
	else
		return ZERO;
}

/* Functie folosita pentru a prelucra */
/* un fisier ce trebuie inclus */

int do_include(hash_struct *map, char *result, int *flag, char *path)
{
	FILE *f;
	char *filename, *delim = "\"";
	char *buf = malloc(BUF_SIZE);
	char *header = malloc(BUF_SIZE);

	if (buf == NULL)
		return ERROR_MALLOC;

	filename = strtok(NULL, delim);
	if (strcmp(path, "") != 0) {
		strcpy(header, path);
		strcat(header, "/");
	} else {
		strcpy(header, "_test/inputs/");
	}
	header = strcat(header, filename);
	f = fopen(header, "r");
	free(header);
	if (f == NULL) {
		free(buf);
		return NOT_OK;
	}
	while (fgets(buf, BUF_SIZE, f))
		replace(buf, map, result, flag, path);

	free(buf);
	fclose(f);

	return OK;
}

/* Functie folosita pentru a inlocui */
/* un sir interminabil de if - else-uri */
int switch_line(char *token)
{
	if (strcmp(token, "\n") == 0)
		return NEW_LINE;

	if (strcmp(token, "#define") == 0)
		return DEFINE;

	if (strcmp(token, "#undef") == 0)
		return UNDEF;

	if (strcmp(token, "#if") == 0)
		return IF;

	if (strcmp(token, "#endif\n") == 0)
		return -EN;

	if (strcmp(token, "#else\n") == 0)
		return -ELS;

	if (strcmp(token, "#elif") == 0)
		return -ELF;

	if (strcmp(token, "#ifdef") == 0)
		return IFDEF;

	if (strcmp(token, "#ifndef") == 0)
		return IFNDEF;

	if (strcmp(token, "#include") == 0)
		return INCLUDE;

	return DEFAULT;
}


/* In aceasta functie se face interpretarea fiecarei linii */
/* Se verifica daca liniile contin string-uri precum */
/* #define X si se face interpretarea acestora in mod corespunzator */
int replace(char *str, hash_struct *map, char *result, int *flag, char *path)
{
	int err = OK, found, instruction;
	char *token, *new_value, *delim = " %+-*()\t;=<>.,[]/";
	char *temp = (char *)malloc(strlen(str) + 1);
	//FILE *out;

	//out = fopen("outputfile", "a+");

	if (temp == NULL)
		return ERROR_MALLOC;

	strcpy(temp, str);
	token = strtok(str, delim);
	instruction = switch_line(token);

	switch (instruction) {
	case NEW_LINE:
		strcpy(result, "");
		break;

	case DEFINE:
		err = add_define_to_map(map);
		strcpy(result, "");
		break;

	case UNDEF:
		err = delete_define_from_map(map);
		strcpy(result, "");
		break;

	case IF:
		*flag = check_if(map);
		strcpy(result, "");
		break;

	case -EN:
		*flag = 1;
		strcpy(result, "");
		break;

	case -ELS:
		if (*flag == 0)
			*flag = 1;
		else
			*flag = 0;

		strcpy(result, "");
		break;

	case -ELF:
		if ((*flag) == 0)
			(*flag) = check_if(map);
		strcpy(result, "");
		break;

	case IFDEF:
		(*flag) = find_defined_token(map);
		strcpy(result, "");
		break;

	case IFNDEF:
		(*flag) = find_defined_token(map);
		(*flag) = change_value(*flag);
		strcpy(result, "");
		break;

	case INCLUDE:
		err = do_include(map, result, flag, path);
		if (err == NOT_OK) {
			free(temp);
			return NOT_OK;
		}
		strcpy(result, "");
		break;

	case DEFAULT:
		found = 0;
		while (token != NULL) {
			new_value = check_token(token, map);
			if (new_value != NULL) {
				found = 1;
				rep_tok(token, temp, new_value, result);
			}

			if (found == 0)
				strcpy(result, temp);

			token = strtok(NULL, delim);
			if (*flag == 0)
				strcpy(result, "");
		}
		free(token);
		break;
	}
	printf("%s", result);
	free(temp);
	return err;
}


/* Se citeste linie cu linie fisierul de intrare */
/* Se trimite la prelucrat fiecare linie, */
/* iar la final se afiseaza rezultatul */
/* Tot in aceasta functie sunt transformate */
/* intr-o singura functie definitiile */
/* facute pe mai multe linii */
int print_file(char *filename, hash_struct *map, char *path)
{
	char *buf = malloc(BUF_SIZE);
	char *buf2 = malloc(BUF_SIZE);
	char *buf_temp = malloc(BUF_SIZE);
	char *delim = "\\\n";
	char *delim2 = "  ";
	char *result = malloc(BUF_SIZE);
	int err = OK, flag = 1;
	FILE *f;

	if (buf == NULL)
		return ERROR_MALLOC;

	if (buf2 == NULL)
		return ERROR_MALLOC;

	if (buf_temp == NULL)
		return ERROR_MALLOC;

	if (result == NULL)
		return ERROR_MALLOC;

	f = fopen(filename, "r");
	if (f == NULL)
		return NOT_OK;

	while (fgets(buf, BUF_SIZE, f)) {
		buf = strremove(buf, delim);
		while (strchr(buf, '\n') == NULL) {
			fgets(buf2, BUF_SIZE, f);
			buf2 = strremove(buf2, delim);
			buf2 = strremove(buf2, delim2);
			strcat(buf, buf2);
		}

		err = replace(buf, map, result, &flag, path);
		if (err != OK)
			return err;
		//printf("%s", result);
	}

	free(buf);
	free(buf2);
	free(buf_temp);
	free(result);
	fclose(f);

	return err;

}

/* Se adauga in Map valoriile primite */
/* de la linia de comanda cu flagul -D */
void add_param_to_map(char *str, hash_struct *map)
{
	char *delim = " =";
	char *symbol;
	char *value;
	char *equal;

	equal = strchr(str, '=');
	if (equal != NULL) {
		symbol = strtok(str, delim);
		value = strtok(NULL, delim);
	} else {
		symbol = strtok(str, delim);
		value = "";
	}
	add(symbol, value, map);
}

/* Aici se face interpretarea parametriilor */
/* primiti de la linia de comanda */
int decode_param(int argc, char **argv, hash_struct *map)
{
	int i, err;
	int found = 0;
	char *temp;
	char *filename = malloc(BUF_SIZE);
	char *output = malloc(BUF_SIZE);
	char *path = malloc(BUF_SIZE);

	if (filename == NULL) {
		free(filename);
		return ERROR_MALLOC;
	}
	strcpy(path, "");
	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-' && argv[i][1] == 'D') {
			if (strlen(argv[i]) <= 2) {
				i++;
				add_param_to_map(argv[i], map);
			} else {
				temp = strdup(argv[i] + 2);
				add_param_to_map(temp, map);
				free(temp);
				}
		} else {
			if (argv[i][0] == '-' && argv[i][1] == 'I') {
				if (strlen(argv[i]) <= 2) {
					i++;
					strcpy(path, argv[i]);
				} else {
					strcpy(path, argv[i] + 2);
					}
			} else {
				found++;
				if (found == 1) {
					strcpy(filename, argv[i]);
					found++;
				} else {
					if (found == 2) {
						strcpy(output, argv[i]);
						found++;
					}
				}
			}
		}
	}

	if (found > 2)
		return NOT_OK;

	err = print_file(filename, map, path);
	if (err == NOT_OK) {
		free(filename);
		return NOT_OK;
	}
	free(output);
	free(path);
	free(filename);
	return OK;
}
