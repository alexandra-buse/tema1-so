#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hash.h"

/* Se aloca spatiu pentru HashMap */
int create_hash(hash_struct **hash_map)
{
	int i;
	*hash_map = malloc(sizeof(hash_struct));
	if (*hash_map == NULL)
		return ERROR_MALLOC;
	(*hash_map)->length = HASH_SIZE;
	(*hash_map)->head = malloc(sizeof(node *) * HASH_SIZE);

	if ((*hash_map)->head == NULL) {
		free(*hash_map);
		return ERROR_MALLOC;
	}

	for (i = 0; i < HASH_SIZE; i++)
		(*hash_map)->head[i] = NULL;

	return OK;
}


/* Algoritmul djb2, putin modificat */
int hash_function(const char *str)
{
	int hash = VALUE_HASH;
	int c;

	while ((c = *str++) != 0)
		hash = hash * VALUE_MULTIPLIER + c;

	return (hash % HASH_SIZE);
}


/*Functie pentru cautarea unei valori */
/* in HashMap */
node *find(char *search_term, hash_struct *hash_map)
{
	int key = hash_function(search_term);
	int i;
	node *p;

	for (i = 0; i < hash_map->length; i++) {
		if (hash_map->head[i] != NULL)
			if (i == key) {
				p = hash_map->head[key];
				return p;
			}
	}
	return NULL;
}

/*Functie pentru adaugarea unei valori */
/* in HashMap */
int add(char *symbol, char *value_to_add, hash_struct *hash_map)
{
	int key, str_len;
	node *p;
	node *new_node = malloc(sizeof(node));

	if (new_node == NULL)
		return ERROR_MALLOC;

	str_len = strlen(value_to_add) + 1;

	new_node->value = malloc(str_len);
	if (new_node->value == NULL) {
		free(new_node);
		return ERROR_MALLOC;
	}

	memcpy(new_node->value, value_to_add, str_len);
	key = hash_function(symbol);

	p = hash_map->head[key];
	if (p == NULL) {
		hash_map->head[key] = new_node;
		hash_map->head[key]->next = NULL;
		return OK;
	}

	for (p = hash_map->head[key]; p->next; p = p->next)
		;
	new_node->next = NULL;
	p->next = new_node;
	return OK;
}

/*Functie pentru stergerea unei valori */
/* din HashMap */
int rem(char *value, hash_struct  *hash_map)
{
	int key;
	node *p;
	node *np = find(value, hash_map);

	if (np == NULL)
		return ERROR_MALLOC;

	key = hash_function(value);
	if (hash_map->head[key] == np) {
		hash_map->head[key] = np->next;
		free(np->value);
		free(np);
		return OK;
	}

	for (p = hash_map->head[key]; p->next != np; p = p->next)
		;
	p->next = np->next;
	free(np->value);
	free(np);
	return OK;
}

/*Functie pentru golirea tuturor */
/* valorilor din HashMap */
void clear(hash_struct *hash_map)
{
	int i;
	node *aux, *np;

	for (i = 0; i < hash_map->length; i++)
		if (hash_map->head[i] != NULL) {
			np = hash_map->head[i];
			for (; np != NULL;) {
				aux = np;
				np = np->next;
				free(aux->value);
				free(aux);
			}
			hash_map->head[i] = NULL;
		}
}

/*Se elibereaza spatiul alocat pentru HashMap*/
void free_hash(hash_struct *hash_map)
{
	clear(hash_map);
	free(hash_map->head);
	free(hash_map);
}
