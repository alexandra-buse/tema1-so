#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "hash.h"
#include "utils.h"


int main(int argc, char **argv)
{
	hash_struct *hash_map;
	int err = OK;

	err = create_hash(&hash_map);
	if (hash_map == NULL)
		exit(EXIT_FAILURE);

	err = decode_param(argc, argv, hash_map);
	if (err == NOT_OK)
		return NOT_OK;


	free_hash(hash_map);
	return ZERO;

}
